// Chủ đề: Xử lí mảng nâng cap
// B13
var prompt = require("prompt-sync")();
var n = Number(prompt("So phan tu cua mang: "));
var mang = [];
for (let i = 0; i < n; i++) {
    mang.push(Number(prompt("Nhap so: ")));
}
var taphop = {};
for (let y of mang) {
    if (!(y in taphop)) {
        taphop[y] = 1;
    } else {
        taphop[y] += 1;
    }
}
var key = Object.keys(taphop);
key = key.sort()
if (key.length < 10) {
    console.log("Khong ton tai phan tu thu 10");
} else {
    console.log("Phan tu lon thu 10: ", key[key.length - 10]);
}