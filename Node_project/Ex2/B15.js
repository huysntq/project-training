// Chủ đề: Xử lí mảng nâng cao
// B15
var prompt = require("prompt-sync")();
var n = Number(prompt("So phan tu cua mang: "));
var mang = [];
for (let i = 0; i < n; i++) {
    mang.push(Number(prompt("Nhap so: ")));
}
var sum = 0;
var dem = 0;
for (let y of mang) {
    if (y % 2 == 0) {
        sum += y;
        dem += 1;
    }
}
console.log("Gia tri trung binh cac phan tu chan:", sum/dem);